import pygame
from random import randint

fps = 100
clock = pygame.time.Clock()
pygame.init()
size = (400, 300)
width, height = size
screen = pygame.display.set_mode(size)

circles = []  # список с координатами всех кругов на экране
circle_colors = []  # список с цветами кругов


# создание круга
def draw_circle(event: pygame.event):
    x, y = event.pos
    r, g, b = randint(0, 255), randint(0, 255), randint(0, 255)
    circles.append([x, y])
    circle_colors.append((r, g, b))


while True:
    pygame.display.update()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()
        if event.type == pygame.MOUSEBUTTONDOWN:
            draw_circle(event)
    surface = pygame.Surface(size)
    # рисуем круги на новом холсте
    for i in range(len(circles)):
        pygame.draw.circle(surface, circle_colors[i], circles[i], 10)
        x, y = circles[i]
        # если круг не достиг нижней границы, увеличиваем координату
        if y <= 290:
            circles[i] = x, y + 1
    # перерисовка с холста на экран
    screen.blit(surface, (0, 0))
    clock.tick(fps)
