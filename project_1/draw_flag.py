import pygame

WHITE = (255, 255, 255)
BLUE = (0, 57, 166)
RED = (213, 43, 30)

pygame.init()
surface = pygame.display.set_mode((400, 400))

while True:
    for i in pygame.event.get():
        if i.type == pygame.QUIT:
            exit()
    pygame.draw.line(surface, WHITE, [10, 10], [10, 300], 10)
    pygame.draw.rect(surface, WHITE, (20, 10, 150, 33))
    pygame.draw.rect(surface, BLUE, (20, 43, 150, 33))
    pygame.draw.rect(surface, RED, (20, 76, 150, 33))
    pygame.display.update()
